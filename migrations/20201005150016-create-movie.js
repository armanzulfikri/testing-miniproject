'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Movies', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      title: {
        type: Sequelize.STRING
      },
      director: {
        type: Sequelize.STRING
      },
      genre : {
        type: Sequelize.STRING
      },
      synopsis: {
        type: Sequelize.TEXT
      },
      cast: {
        type: Sequelize.STRING
      },
      rate: {
        type: Sequelize.STRING
      },
      poster: {
        type: Sequelize.STRING
      },
      trailer: {
        type: Sequelize.STRING
      },
      realeased_date: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Movies');
  }
};