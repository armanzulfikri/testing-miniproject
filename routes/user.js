const { Router } = require('express');
const router = Router();
const UserController = require('../controllers/User')
const {uploadUser} = require('../middlewares/multer')
const Auth = require('../middlewares/auth');
const { getUserID } = require('../controllers/User');

router.get('/list', Auth.authentication,UserController.getUser);
router.get('/id', Auth.authentication,UserController.getUserById)
router.get('/edit/',Auth.authentication, UserController.editFormUser);
router.put('/edit/',uploadUser.single('profileImage'),Auth.authentication, UserController.editUser);
router.delete('/', Auth.authentication, UserController.deleteUser);
router.get('/data/:id',UserController.getUserID);

 module.exports = router;


