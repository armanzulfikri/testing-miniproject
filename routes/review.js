const { Router } = require('express');
const router = Router();
const ReviewController = require('../controllers/Review')

const Auth = require('../middlewares/auth');

router.get('/', ReviewController.getReview)
router.get('/list', Auth.authentication, ReviewController.listReview )
router.post('/:MovieId', Auth.authentication, ReviewController.addReview)
router.put('/edit/:id', Auth.authentication, ReviewController.editReview);
router.delete('/delete/:id',Auth.authentication, Auth.authorization, ReviewController.deleteReview)
router.get('/share', Auth.authentication, ReviewController.share);


module.exports = router;
