const { Router } = require('express');
const router = Router();
const MovieController = require('../controllers/Movie')


router.get('/',MovieController.getMovie);
router.get('/search', MovieController.searchmovie);
router.get('/detail/:id', MovieController.findMovieId);
router.post('/add',MovieController.addMovie);
router.get('/category', MovieController.filterGenre);
router.delete('/delete', MovieController.deleteMovie);
router.put('/edit/:id', MovieController.editMovie);


 module.exports = router;
