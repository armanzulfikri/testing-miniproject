const { Router } = require('express');
const router = Router();

const UserController = require('../controllers/User');

const { uploadUser } = require('../middlewares/multer');

const UserRoutes = require('./user')
const MovieRoutes = require('./movie')
const ReviewRoutes = require('./review')

router.get('/', (req,res)=>{
    res.status(200).json({
        message : "This is home page thanks."
    })
});

router.post('/register', uploadUser.single('profileImage'), UserController.register);
router.post('/login', UserController.login);

// Routes
router.use('/user', UserRoutes)
router.use('/movie', MovieRoutes);
router.use('/review', ReviewRoutes)


module.exports = router;
