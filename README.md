# TeamG-Backend - Movie Apps


# Movie app
creating movie app is an application to manage your assets. This app has : RESTful endpoint for e-comerce-cms app CRUD operation

link web
# https://damp-dawn-67180.herokuapp.com/

RESTful endpoint


# TeamD-Backend
RESTful endpoints
```
```

## POST /register <link> https://damp-dawn-67180.herokuapp.com/register
register User

## Request Header 
{not needed}

## Request Body 
{nama,email,password}
not needed
```
Response (200) 
```json
[
    {
        "nama" : "<namq>",
        "email" : "<email>",
        "password" : "<password>",

    }
]   
```
```
Response (400 - Bad Request) 
    {
        "msg": "<returned error message>"
    }
```

## POST /login <link> https://damp-dawn-67180.herokuapp.com/login

## Request Header 
not needed

## Request Body 
{email,password}
```
```
Response (200)
    {
        "access_token": "<asset access_token>"
    }
```
```

Response (500 - Bad Request){
  "message": "<returned error message>"
}

```
```
## GET /user/list  <link> https://damp-dawn-67180.herokuapp.com/user/list
get user by all user
     
## Request Header
"<asset access_token>"

## Request Body
"not needed"

```
Response (200)
    
    {
        "id": "<asset id>",
        "nama": "<asset nama>",
        "email": "<asset email>",
        "password": "<asset password>",
        "profileImage": "<asset image>",
        "createdAt": "<asset createdAt>",
        "updatedAt": "<asset updatedAt>"
    },
        {
        "id": "<asset id>",
        "nama": "<asset nama>",
        "email": "<asset email>",
        "password": "<asset password>",
        "profileImage": "<asset image>",
        "createdAt": "<asset createdAt>",
        "updatedAt": "<asset updatedAt>"
    }
```
```
Response (400 - Bad Request)
    {
        "msg": "Token not found"
    }
```
```
```
## GET /user/id <link> https://damp-dawn-67180.herokuapp.com/user/list
get user by id 
    
## Request Header
"<asset access_token>"


## Request Body
"not needed"


```
Response (200)
    
    {
        "id": "<asset id>",
        "nama": "<asset nama>",
        "email": "<asset email>",
        "password": "<asset password>",
        "profileImage": "<asset image>",
        "createdAt": "<asset createdAt>",
        "updatedAt": "<asset updatedAt>"
    },
  Review {
      "UserId" : "<User Id>",
      "MovieId": "<Movie Id>",
      "rating": "<rating>",
      "comment": "<comment>",
      "share": "<share>",
  }
```
```
Response (400 - Bad Request)
    {
        "msg": "Token not found"
    }
```
```
```

## PUT /user/edit  <link> https://damp-dawn-67180.herokuapp.com/user/edit
Editing User 

## Request Headers
needed access_token

## Request Body
{nama,profileImage}
```
Respon(200)
{
  "name": "<asset fullname>",
  "profileImage": "<asset image>"
}
```
```
Respon(404)
{
  "msg": "User not Found"
}
```
## Delete /user/delete/ <link> https://damp-dawn-67180.herokuapp.com/user/delete
Deleting User

## Request Header :
{"access_token": "<your access token>"}

request Data :
{ id }

-Response (202 - user deleted)
```json
```
- Response (500 - Bad Request)
```json
{
  "<returned error message>"
}

```

## Get movie/ <link> https://damp-dawn-67180.herokuapp.com/movie/
get all movie
    
## Header
not needed

## Request Body
not needed

## Parameter Query
 need page query 


- Response (200 - OK)
```json
[
    {
        "id": "<asset id>",
        "title": "<asset title>",
        "director": "<asset director>" ,
        "genre": "<asset genre>",
        "synopsis": "<asset synopsis>",
        "cast": "<asset cast>",
        "rate": "<asset rate>",
        "poster": "<asset poster>",
        "trailer": "<asset trailer>",
        "realeased_date": "<asset realeased_date>",
        "updatedAt": "<asset updatedAt>",
        "createdAt": "<asset createdAt>"
    },
      {
        "id": "<asset id>",
        "title": "<asset title>",
        "director": "<asset director>" ,
        "genre": "<asset genre>",
        "synopsis": "<asset synopsis>",
        "cast": "<asset cast>",
        "rate": "<asset rate>",
        "poster": "<asset poster>",
        "trailer": "<asset trailer>",
        "realeased_date": "<asset realeased_date>",
        "updatedAt": "<asset updatedAt>",
        "createdAt": "<asset createdAt>"
    }
]
```
## Get Movie by ID <link> https://damp-dawn-67180.herokuapp.com/movie/detail:id
get movie by ID

## Headers
"not needed"

## Request Body
"Not needed"

## RequestParameter
"params.id"

```
```
Res.Status (200)
Movie
    {
        "id": 2,
        "title": "<asset title>",
        "director": "<asset director> ,
        "genre": "<asset genre>",
        "synopsis": "<asset synopsis>",
        "cast": "<asset cast>",
        "rate": "<asset rate>",
        "poster": "<asset poster>",
        "trailer": "<asset trailer>",
        "realeased_date": "<asset realeased_date>",
        "updatedAt": ""<asset updatedAt>"",
        "createdAt": ""<asset createdAt>""
    }
Review {
        "UserId" : "<UserId>",
        "MovieId": "<Movie Id>",
        "rating": "<Rating>"
        
        "comment" : "<comment>",
        "share": "<share>",
}
```
```
Error (404 - conflict)

    {
        "msg": "Title already exist, try another title."
    }

``
## GET /movie/search <link> https://damp-dawn-67180.herokuapp.com/movie/search
Search Movie by title

## Headers
"not needed"

## Params 
{ search }

- Response (200 - movie found)
```json

Status (200)
    {
        "id": "2",
        "title": "<asset title>",
        "director": "<asset director>" ,
        "genre": "<asset genre>",
        "synopsis": "<asset synopsis>",
        "cast": "<asset cast>",
        "rate": "<asset rate>",
        "poster": "<asset poster>",
        "trailer": "<asset trailer>",
        "realeased_date": "<asset realeased_date>",
        "updatedAt": "<asset updatedAt>",
        "createdAt": "<asset createdAt>"
    }
- Response (500 - Bad Request)
```json
{
  "<returned error message>"
}

```
``
## GET /movie/category <link> https://damp-dawn-67180.herokuapp.com/movie/category
Search Genre in Movie 

## Headers
"not needed"

## Request Params
{genre}
## Request Body
not needed

- Response (200 - movie found)
```json

Status (200)
    {
        "id": "2",
        "title": "<asset title>",
        "director": "<asset director>" ,
        "genre": "<asset genre>",
        "synopsis": "<asset synopsis>",
        "cast": "<asset cast>",
        "rate": "<asset rate>",
        "poster": "<asset poster>",
        "trailer": "<asset trailer>",
        "realeased_date": "<asset realeased_date>",
        "updatedAt": "<asset updatedAt>",
        "createdAt": "<asset createdAt>"
    }
- Response (500 - Bad Request)
```json
{
  "<returned error message>"
}

```
## Post movie/add/ <link> https://damp-dawn-67180.herokuapp.com/movie/add
adding Movie
## Request Header : 
{}

## Request Data :
{ user.data.id }

## Request Body :
{ title, diretor, genre, synopsis, cast, rate, poster,trailer,realeased_date,updatedAt,createdAt }

- Response (200 - OK)
```json
[
  {
        "id" : "<movie id>",
        "title": "<asset title>",
        "director": "<asset director>" ,
        "genre": "<asset genre>",
        "synopsis": "<asset synopsis>",
        "cast": "<asset cast>",
        "rate": "<asset rate>",
        "poster": "<asset poster>",
        "trailer": "<asset trailer>",
        "realeased_date": "<asset realeased_date>",
        "updatedAt": "<asset updatedAt>",
        "createdAt": "<asset createdAt>"
  }
]
```
- Response (500 - Bad Request)
```json
{
  "<returned error message>"
}
```
## Delete movie/delete/:id : Deleting movie <link> https://damp-dawn-67180.herokuapp.com/movie/delete
Delete Movie
## Request Authorization :  
user

## Request Header : 
{"access_token": "<your access token>"}

## Request Data :
{ id }

## Request Body :
no needed

- Response (200 - movie deleted)
```json

```
- Response (500 - Bad Request)
```json
{
  "<returned error message>"
}
```
## PUT /movie/edit/:id <link> https://damp-dawn-67180.herokuapp.com/movie/:id
Edit Movie

## Request Header : 
{"access_token": "<your access token>"}

## Request Body :
{ title, diretor, genre, synopsis, cast, rate, poster,trailer,realeased_date,updatedAt,createdAt }

- Response (200 - OK)
```json
[
  {
        "id" : "<movie id>",
        "title": "<asset title>",
        "director": "<asset director>" ,
        "genre": "<asset genre>",
        "synopsis": "<asset synopsis>",
        "cast": "<asset cast>",
        "rate": "<asset rate>",
        "poster": "<asset poster>",
        "trailer": "<asset trailer>",
        "realeased_date": "<asset realeased_date>",
        "updatedAt": "<asset updatedAt>",
        "createdAt": "<asset createdAt>"
  }
]
```
- Response (500 - Bad Request)
```json
{
  "<returned error message>"
}
```

## GET/review/      <link> https://damp-dawn-67180.herokuapp.com/review/
Get review all include User and Movie
Request Header : 
{"access_token": "<your access token>"}

Request Body :
{ No needed }
- Response (200 - OK)
json
[
    {
      "UserId": "<User Id>",
        "nama": "<uploaded nama>",
        "email": "<character email>",
        "profileImage": "<character profileImage>",
      "MovieId": "<Movie Id>",
        "id" : "<movie id>",
        "title": "<asset title>",
        "director": "<asset director>" ,
        "genre": "<asset genre>",
        "synopsis": "<asset synopsis>",
        "cast": "<asset cast>",
        "rate": "<asset rate>",
        "poster": "<asset poster>",
        "trailer": "<asset trailer>",
        "realeased_date": "<asset realeased_date>",
        "updatedAt": "<asset updatedAt>",
        "createdAt": "<asset createdAt>"
        }
    },
]
- Response (500 - Bad Request)
json
{
  "<returned error message>"
}


## Get review/list  <link> https://damp-dawn-67180.herokuapp.com/review/list
Get all review list by id

## Request Header : 
{"access_token": "<your access token>"}

## Request Body :
{ No needed }
- Response (200 - OK)
json
[
    {
      "id" : "<review id>",
      "UserId": "<User Id>",
      "MovieId": "<Movie Id>",
      "rating": "<Rating>",
      "comment": "<comment>",
      "share": "<share>",

        }
    },
]
- Response (500 - Bad Request)
json
{
  "<returned error message>"
}
```
```

## delete review/delete/:id <link> https://damp-dawn-67180.herokuapp.com/review/delete/:id
delete Review
## Request Header : 
{"access_token": "<your access token>"}

## Request Body :
{ No needed }

## Param  :
MovieiD

- Response (200 - OK)
json
[
 msg : "Review Has been deleted";
]
- Response (500 - Bad Request)
json
{
  "<returned error message>"
}

```
```

## POST review/:MovieID <link> https://damp-dawn-67180.herokuapp.com/review/:MovieID
AddReview

## Request Header : 
{"access_token": "<your access token>"}

## Request Body :
{ rating,comment,share}

## Params : 
{Movie:id}

- Response (200 - OK)
json
[
     {
      "id" : "<review id>",
      "UserId": "<User Id>",
      "MovieId": "<Movie Id>",
      "rating": "<Rating>",
      "comment": "<comment>",
      "share": "<share>",
    },
]
[
  msg:Review Has been added
]
- Response (500 - Bad Request)
json
[

]

```
```
## PUT review/edit/:id/ <link> https://damp-dawn-67180.herokuapp.com/review/:id/edit

## Request Header : 
{"access_token": "<your access token>"}

## Request Body :
{ rating,comment,share}

## Params : 
{Reviews:id}

- Response (200 - OK)
json
[
  1
]
 msg : "Review Has beeen edited";
]
- Response (500 - Bad Request)
json
[
    {
        }
    },
]

```
```

## Get review/share <link> https://damp-dawn-67180.herokuapp.com/review/share

get useid review with share true

## Request Header : 
{"access_token": "<your access token>"}

## Request Body :
{ rating,comment,share}

## Params : 
{Movie:id}

- Response (200 - OK)
json
[
      "id" : "<review id>",
      "UserId": "<User Id>",
      "share": "<share>",
]
- Response (500 - Bad Request)
json
[
    {
        }
    }, 
]

```
```
