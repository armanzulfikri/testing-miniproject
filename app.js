const express = require('express');
const app = express();
const cors = require('cors')
require('dotenv').config();

const PORT = process.env.PORT || 4000;
const router = require('./routes');

//Middlewares
app.use(cors())
app.use('/uploads', express.static('uploads'));
app.use(express.json());
app.use(express.urlencoded({ extended: true}));

//Routes
app.use(router);

app.listen(PORT, () => {
    console.log(`Server is running at port : ${PORT}`);
});
