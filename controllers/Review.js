const { Review, User, Movie } = require('../models')

class ReviewController {
    static async getReview(req, res) {
        try {
            const result = await Review.findAll({
                order: [
                    ['id', 'ASC']
                ],
                include : [
                    User,Movie
                ]
            })
            res.status(200).json(result);

        }
        catch (err) {
            res.status(500).json(err);
        }
    }
    static async listReview(req, res) {
        try {
            const result = await Review.findAll({
                order: [
                    ['id', 'ASC']
                ]
            })
            res.status(200).json(result);
        }
        catch (err) {
            res.status(500).json(err);
        }
    }
    static async addReview(req, res) {
        const { rating, comment ,share } = req.body;
        const rated = parseInt(rating);
        const UserId = req.userData.id;
        const MovieId = req.params.MovieId;
        // console.log(req.body);
        try {
            const found = await Review.findOne({
                where: {
                    UserId, MovieId
                }
            })
            if (found) {
                res.status(409).json({
                    msg: "Your review for this movie is already exist! Try another movie."
                })
            } else {
                const review = await Review.create({
                    UserId, MovieId, rating : rated, comment,share
                })

                res.status(201).json({
                    review,
                    msg : "Review has been add"
                })
            }
        } catch (err) {
            res.status(500).json(err)
        }
    }
    static async deleteReview(req, res) {
        const UserId = req.userData.id;
		const id = req.params.id;

        try {
            const result = await Review.destroy({
                where: { UserId, id}
            });
            res.status(200).json({
                result,
                msg: "Review has been Deleted"
            })
        } catch (err) {
            res.status(500).json(err)
        }
    }

    static async editReview(req, res) {
        const{ id }= req.params;
        const UserId = req.userData.id
        const {  rating, comment,share } = req.body;
        const rated = parseInt(rating);
        try {
            const result = await Review.update({
                rating : rated,
                comment,
                share
            }, {
                where: { UserId,id }
            });
            res.status(200).json({
                result,
                msg : "Review has been edited"
            });
        } catch (err) {
            res.status(500).json(err);
        }
    }

    static async share(req, res) {
        const UserId = req.userData.id
        try {
            const reviews = await Review.findAll({
                where: { UserId, share: true }
             });
             res.status(200).json(reviews);
         } catch (err) {
            res.status(500).json(err);
        }
        }
    


}
module.exports = ReviewController; 