const {Movie , Review } = require ('../models');
const { Op } = require('sequelize');
const { paginate } = require('../helpers/paginate');

class MovieController {
    static async getMovie (req, res){
        try {
            const page = Number(req.query.page);
            const limit = 10;    
            const movies= await Movie.findAll({
                order: [
                    ['id', 'ASC']
                ]
            });
            const result = paginate(page, limit, movies);
            res.status(200).json(result);
        } catch (err) {
            res.status(500).json(err);
        }
    }
    static async findMovieId (req, res){
        const id = req.params.id;
        try {
            const detail = await Movie.findOne({
                where: {
                    id
                }
            });
            const reviewmovie = await Review.findAll({
                where: {
                    MovieId: id
                }
            })
            res.status(200).json({detail,reviewmovie})
        }catch (err) {
            res.status(500).json(err);
        }
    }
    static async searchmovie (req, res) {
        const { search } = req.query;
        try {
            const found = await Movie.findAll({
                where: {
                    title: {
                        [Op.iLike]: '%' + search + '%'
                    }
                }
            })
            if(found){
                res.status(200).json(found);
            } else {
                res.status(409).json({
                    msg: "Movie is not available!"
                });
            }
        }catch (err) {
            res.status(500).json(err);
        }
    }
    static async filterGenre (req, res) {
        const { genre } = req.query;
        console.log(req.query);
        try {
            const page = Number(req.query.page);
            const limit = 10; 
            const filter = await Movie.findAll({
                where: {
                    genre: {
                        [Op.iLike] : '%' + genre + '%'
                    }
                }
            });
            const result = paginate(page, limit, filter);
            res.status(200).json(result);
        } catch (err) {
            res.status(500).json(err);
        }


    }

    static async addMovie (req, res){
        const{title,director,genre,synopsis,cast,rate,poster,trailer,realeased_date} =  req.body;
        
        try {
                const movie = await Movie.create({
                    title,
                    director,
                    genre,
                    synopsis,
                    cast,
                    rate,
                    poster,
                    trailer,
                    realeased_date
                    
                })
                res.status(201).json({
                    movie,
                    msg : "Movie  successfully added"
                });
            
        }catch (err) {
            res.status(500).json(err);
        }
    }

    static async editMovie (req, res){
        const id = req.params.id;
        const{title,director,genre,synopsis,cast,rate,poster,trailer,realeased_date} =  req.body;
        try{
            const movie = await Movie.update({
                title,
                director,
                genre,
                synopsis,
                cast,
                rate,
                poster,
                trailer,
                realeased_date
            },{
                where: { id }
            });
            res.status(200).json({
                movie,
                msg: "Movie edited successfully!"
            }); 
        } catch (err) {
            res.status(500).json(err)
        }

    }
    static async deleteMovie (req, res) {
        const id = req.params.id;
        
        try{
            const hapus = await Movie.destroy({
                where: {
                    id
                }
                
            })
            res.status(200).json({
                hapus,
                msg : "Movie Deleted"
            });
        }catch(err){
            res.status(500).json(err);
        }
    }

}

module.exports = MovieController;
