'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Review extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Review.belongsTo(models.User);
      Review.belongsTo(models.Movie);
    }
  };
  Review.init({
    UserId: DataTypes.INTEGER,
    MovieId: DataTypes.INTEGER,
    rating:{
      type : DataTypes.DOUBLE,
      validate : {
        notEmpty : {
          msg : " Rating must be filled"
        },
        min : 0,
        max : 10
      }
  },
    comment: {
      type : DataTypes.STRING,
      validate : {
        notEmpty : {
          msg : " Comment must be filled"
        }
      }
    },
    share:{
      type : DataTypes.STRING,
      validate : {
        notEmpty : {
          msg : " Share must be filled"
        }
      }
    }
  }, {
    sequelize,
    modelName: 'Review',
  });
  return Review;
};