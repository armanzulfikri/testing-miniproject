'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Movie extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Movie.belongsToMany(models.User, { through: 'models.Review' });

    }
  };
  Movie.init({
    title: {
      type : DataTypes.STRING,
       validate : {
         notEmpty : {
           msg : "Title Movie must be filled"
         }
       }
    },
    director: {
      type : DataTypes.STRING,
       validate : {
         notEmpty : {
           msg : "Director Movie must be filled"
         }
       }
      },
    genre: {
      type: DataTypes.STRING,
      validate : {
        notEmpty : {
          msg : "Genres Movie must be filled"
        }
      }
    },
    synopsis: {
      type: DataTypes.TEXT,
      validate : {
        notEmpty : {
          msg : "Synopsis Movie must be filled"
        }
      }
    },
    cast: {
      type : DataTypes.STRING,
      validate : {
        notEmpty : {
        msg : "Cast Movie Must be filled"
        }
      }
    },
    rate : {
      type : DataTypes.STRING,
      validate : {
        notEmpty : {
          msg : "Rate Must be filled"
        }
      }
    },
    poster: {
      type : DataTypes.STRING,
      validate : {
        notEmpty : {
          msg : "Poster Must be filled"
        },
        isUrl : {
          msg : "Poster must be url"
        }
      }
    },
    trailer: {
      type : DataTypes.STRING,
      validate : {
        notEmpty : {
          msg : "Trailer Must be filled"
        },
        isUrl : {
          msg : "Trailer must be url"
        }
      }
    },
    realeased_date: {
      type : DataTypes.STRING,
      validate : {
        notEmpty : {
          msg : "Realeased Movie must be filled"
        }
      }
    }
  }, {
    sequelize,
    modelName: 'Movie',
  });
  return Movie;
};