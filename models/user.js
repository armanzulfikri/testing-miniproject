'use strict';
const {
  Model
} = require('sequelize');
const { encryptPwd } = require('../helpers/bcrypt');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User.belongsToMany(models.Movie, {through : 'models.Review'});
    }
  };
  User.init({
    nama: {
      type : DataTypes.STRING,
      validate : {
        notEmpty : {
          msg : 'Nama must be filled'
        }
      }
    },
    email: {
      type : DataTypes.STRING,
      validate : {
        notEmpty : {
          msg : 'Email must be filled'
        },
        isEmail : {
          msg : 'must filled correct email'
        }
      }
    },
    password: {
      type :  DataTypes.STRING,
      validate : {
        notEmpty : {
          msg : 'Password must be filled'
        }
      }
    },
    profileImage: DataTypes.STRING
  }, {
    hooks: {
      beforeCreate(user) {
          user.password = encryptPwd(user.password)
          user.profileImage = "https://www.w3schools.com/w3css/img_avatar3.png";
      }
  },
    sequelize,
    modelName: 'User',
  });
  return User;
};